package com.owen.practisearchitecture.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import com.owen.practisearchitecture.DataRepository
import com.owen.practisearchitecture.viewobject.Word

public class WordViewModel : ViewModel() {


    private var index: MutableLiveData<Int> = MutableLiveData()

    init {
        index.postValue(-1)
    }

    fun getWordLiveData(applicationContext: Context): LiveData<List<Word>> = DataRepository.getInstance(applicationContext).getWordList()

    fun setWordIndex(index: Int) {
        this.index.postValue(index)
    }

    fun getWordIndexLiveData() : MutableLiveData<Int> = index

}
