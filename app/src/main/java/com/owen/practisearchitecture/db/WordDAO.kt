package com.owen.practisearchitecture.db

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.owen.practisearchitecture.viewobject.TABLE_NAME_WORD
import com.owen.practisearchitecture.viewobject.Word

@Dao
interface WordDAO {

    @Query("SELECT * FROM $TABLE_NAME_WORD")
    fun getAll(): LiveData<List<Word>>

    @Query("select * from word_table where word_table.word = :word")
    fun getWord(word: String): LiveData<Word>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(word: Word)

    @Update
    fun update(word: Word)

    @Delete
    fun delete(word: Word)

}