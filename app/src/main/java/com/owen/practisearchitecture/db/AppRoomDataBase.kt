package com.owen.practisearchitecture.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.owen.practisearchitecture.viewobject.Word


@Database (
        entities = [Word::class],
        version = 1,
        exportSchema = false
)
abstract class AppRoomDataBase : RoomDatabase() {

    abstract fun wordDAO(): WordDAO

    companion object {
        private val NAME = "PractiseRoomDB"

        @Volatile private var instances: AppRoomDataBase? = null

        fun getInstance(context: Context): AppRoomDataBase =
            instances ?: synchronized(this) {
                buildDatabase(context).also { instances = it }
            }

        private fun buildDatabase(appContext: Context): AppRoomDataBase {
            return Room.databaseBuilder<AppRoomDataBase>(appContext, AppRoomDataBase::class.java, NAME)
                    .fallbackToDestructiveMigration()
                    .build()
        }
    }
}