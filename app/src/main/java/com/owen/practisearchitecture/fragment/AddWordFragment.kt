package com.owen.practisearchitecture.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.owen.practisearchitecture.DataRepository
import com.owen.practisearchitecture.R
import com.owen.practisearchitecture.viewmodel.WordViewModel
import com.owen.practisearchitecture.viewobject.Word
import kotlinx.android.synthetic.main.fragment_add_word.*


class AddWordFragment : Fragment() {

    private var viewModel: WordViewModel? = null
    private var index = -1
    private var items: List<Word>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_word, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // TODO: Use the ViewModel
        viewModel = ViewModelProviders.of(requireActivity()).get(WordViewModel::class.java)

        viewModel?.getWordLiveData(this.requireContext().applicationContext)?.observe(this, Observer<List<Word>> {
            if (it?.isNotEmpty() == true) {
                Log.d("AddWordFragment", "getWordLiveData observe")
                items = it
            }
        })

        viewModel?.getWordIndexLiveData()?.observe(requireActivity(), Observer {
            Log.d("AddWordFragment", "getWordIndex observe it:$it")
            index = it!!.toInt()

            if(index > -1) {
                textinputedittext_word_edit.setText(items!![index].word)
                ratingbar_word_priority.rating = items!![index].priority
            }
        })

        button_word_add.setOnClickListener {
            val word = Word().apply {
                uid = (System.currentTimeMillis() / 1000).toInt()
                word = textinputedittext_word_edit.text.toString()
                priority = ratingbar_word_priority.rating
            }

            DataRepository.getInstance(context!!.applicationContext).insertWord(word)
        }
    }
}
