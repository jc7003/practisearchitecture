package com.owen.practisearchitecture.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.owen.practisearchitecture.DataRepository
import com.owen.practisearchitecture.R
import com.owen.practisearchitecture.viewmodel.WordViewModel
import com.owen.practisearchitecture.viewobject.Word
import kotlinx.android.synthetic.main.word_list_fragment.*


class WordListFragment : Fragment() {

    private var viewModel: WordViewModel? = null
    private var adapter: Adapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.word_list_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(requireActivity()).get(WordViewModel::class.java)

        viewModel?.getWordLiveData(this.requireContext().applicationContext)?.observe(this, Observer<List<Word>> {
            if(it?.isNotEmpty() == true) {
                val word = it[0]
                Log.d("WordListFragment", "observe word: ${word.word}")
                adapter?.swapItem(it)
            }
        })

        viewModel?.getWordIndexLiveData()?.observe(this, Observer {
            Log.d("WordListFragment", "getWordIndexLiveData it:$it")
        })

        recyclerview.adapter = Adapter().also { adapter = it }
    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val uidText = view.findViewById<TextView>(R.id.textView_index)
        val wordText = view.findViewById<TextView>(R.id.textView_word)
        val delete = view.findViewById<TextView>(R.id.button_delete)
    }

    inner class Adapter : RecyclerView.Adapter<ViewHolder>() {

        private val items: ArrayList<Word> = ArrayList()

        override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_word, viewGroup, false))
        }

        override fun getItemCount(): Int {
            return items.size
        }

        override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
            viewHolder.uidText.text = items[position].uid.toString()
            viewHolder.wordText.text = items[position].word

            viewHolder.itemView.setOnClickListener {
                viewModel?.setWordIndex(position)
            }

            viewHolder.delete.setOnClickListener {
                DataRepository.getInstance(context!!.applicationContext).deleteWord(items[position])
            }
        }

        fun swapItem(items: List<Word>){
            this.items.clear()
            this.items.addAll(items)
            this.notifyDataSetChanged()
        }
    }
}
