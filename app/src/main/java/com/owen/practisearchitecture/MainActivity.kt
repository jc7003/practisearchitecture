package com.owen.practisearchitecture

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.util.TypedValue



class MainActivity : AppCompatActivity() {

    private var isTouched = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val view: View = findViewById(R.id.view_divider)

        val statusBarSize = getStatusBarHeight()
        val actionBarSize = getActionBarHeight()
        val fullHeight = this.resources.displayMetrics.heightPixels - actionBarSize

        val layoutParams = view.layoutParams as ConstraintLayout.LayoutParams
        Log.d("MainActivity", "layoutParams.verticalBias: ${layoutParams.verticalBias}")
        layoutParams.verticalBias

        view.setOnTouchListener { v, event ->
            Log.d("MainActivity", "view_divider touch listener")
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    isTouched = true
                    view.setBackgroundColor(Color.parseColor("#77999900"))
                }
                MotionEvent.ACTION_MOVE -> {
                    Log.d("MainActivity", "event.rawY: ${event.rawY}")

                    val layoutParams = view.layoutParams as ConstraintLayout.LayoutParams
                    view.height/2
                    layoutParams.verticalBias =  (event.rawY - actionBarSize - statusBarSize - view.height / 2)  / fullHeight
                    view.layoutParams = layoutParams
                    view.invalidate()
                    Log.d("MainActivity", "view_layoutParams.verticalBias:" + layoutParams.verticalBias)
                }
                MotionEvent.ACTION_UP -> {
                    isTouched = false
                    view.setBackgroundColor(Color.parseColor("#77009900"))
                }
            }

            return@setOnTouchListener isTouched
        }
    }

    private fun getStatusBarHeight(): Int {
        var result = 0
        val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            result = resources.getDimensionPixelSize(resourceId)
        }
        return result
    }

    private fun getActionBarHeight(): Int {
        val tv = TypedValue()
        var actionBarHeight = 0
        if (theme.resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, resources.displayMetrics)
        }

        return actionBarHeight
    }
}
