package com.owen.practisearchitecture

import android.os.Handler
import android.os.Looper
import android.support.annotation.NonNull
import java.util.concurrent.Executor
import java.util.concurrent.Executors


private val diskIO: Executor = Executors.newSingleThreadExecutor()

private val networkIO: Executor = Executors.newFixedThreadPool(5)

private val mainThread: Executor = MainThreadExecutor()

private class MainThreadExecutor : Executor {
    private val mainThreadHandler = Handler(Looper.getMainLooper())

    override fun execute(@NonNull command: Runnable) {
        mainThreadHandler.post(command)
    }
}


fun runDiskIOThread(f: () -> Unit) {
    diskIO.execute(f)
}

fun runNetworkIoThread(f: () -> Unit) {
    networkIO.execute(f)
}

fun runMainThread(f: () -> Unit) {
    mainThread.execute(f)
}