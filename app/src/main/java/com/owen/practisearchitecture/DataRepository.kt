package com.owen.practisearchitecture

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.owen.practisearchitecture.db.AppRoomDataBase
import com.owen.practisearchitecture.db.WordDAO
import com.owen.practisearchitecture.viewobject.Word

class DataRepository(context: Context) {

    private val wordDAO: WordDAO

    init {
        val roomDataBase = AppRoomDataBase.getInstance(context)
        wordDAO = roomDataBase.wordDAO()
    }

    fun getWordList(): LiveData<List<Word>> = wordDAO.getAll()

    fun insertWord(word: Word) {
        runDiskIOThread {
            wordDAO.insert(word)
        }
    }

    fun upsdateWord(word: Word) {
        runDiskIOThread {
            wordDAO.update(word)
        }
    }

    fun deleteWord(word: Word) {
        runDiskIOThread {
            wordDAO.delete(word)
        }
    }

    companion object {
        private var instance: DataRepository? = null

        fun getInstance(context: Context) = instance
                ?: synchronized(this) { DataRepository(context).also { instance = it } }
    }
}