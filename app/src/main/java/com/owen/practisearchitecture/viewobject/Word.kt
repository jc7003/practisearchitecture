package com.owen.practisearchitecture.viewobject

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

const val TABLE_NAME_WORD = "word_table"

@Entity(tableName = "$TABLE_NAME_WORD")
class Word {

    //primary key
    @PrimaryKey
    var uid: Int = 0

    @ColumnInfo(name = "word")
    var word: String? = null

    @ColumnInfo(name = "priority")
    var priority: Float = 0f
}