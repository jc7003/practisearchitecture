package com.owen.practisearchitecture

import android.app.Application
import com.owen.practisearchitecture.db.AppRoomDataBase

class AppApplication : Application() {

    override fun onCreate() {
        super.onCreate()

    }

    fun getDatabase(): AppRoomDataBase {
        return AppRoomDataBase.getInstance(this)
    }
}